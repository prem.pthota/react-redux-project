import { shallow } from 'enzyme';
import App from './App'

describe('With Enzyme', () => {
  it('Test case for H1 tag', () => {
    const app = shallow(
      <App />
    )
    expect(app.find('h1').text()).toEqual('Hello World')
  })
})